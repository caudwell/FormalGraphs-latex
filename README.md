# LaTeX package to plot Formal Graphs

This package aims at giving tools to conveniently plot Formal Graphs with LaTeX.


## Usage for flat graphs (global level only)

Put in the preamble of your document:

`\usepackage{formalgraphs}`

Then create a Formal Graph environment using 
`\begin{formalgraph}` ... `\end{formalgraph}`

Warning: it is currently mandatory to call nodes *before* operators.

### Commands
Within a `formalgraph` environment the following commands are available:

* Nodes:
 * `\base{<variable>}` : Base quantity
 * `\effort{<variable>}` : Effort
 * `\flow{<variable>}` : Flow
 * `\impulse{<variable>}` : Impulse
 
* Corresponding node labels:
 * `\baselabel{<name>}`
 * `\effortlabel{<name>}`
 * `\flowlabel{<name>}`
 * `\impulselabel{<name>}`
 
* Properties:
 * `\capac{<variable>}` : Capacitance
 * `\elast{<variable>}` : Elastance 
 * `\induc{<variable>}` : Inductance
 * `\reluc{<variable>}` : Reluctance
 * `\conduc{<variable>}` : Conductance
 * `\resist{<variable>}` : Resistance
 * `\composedCG{<variable>}` : Composed path Capacitance/Conductance
 * `\composedLG{<variable>}` : Composed path Reluctance/Resistance
 
* Time:
 * `\evoL{<variable>}` : Time evolution (left side)
 * `\evoR{<variable>}` : Time evolution (right side)
 * `\invoL{<variable>}` : Time involution (left side)
 * `\invoR{<variable>}` : Time involution (right side)
 * `\timeX` : Blocking time evolution (case of pole)

### Variables
The variables are composed of a prefix (4 letters) indicating the variety of energy, followed by a suffix (generally one capital letter) indicating the category of variable. This gives a command of the form `\aaaaA`, to use in a math environment (use `$` before and after).

The available prefixes are:

* `gene`: Generic form of energy
* `tran`: Translation mechanics
* `rota`: Rotation mechanics
* `surf`: Surface energy
* `hydr`: Hydrodynamics
* `grav`: Gravitational energy
* `elec`: Electrodynamics
* `magn`: Magnetism
* `pola`: Electric polarization
* `corp`: Corpuscular energy
* `chim`: Physical chemistry
* `reco`: Corpuscular reaction
* `rech`: Chemical reaction
* `ther`: Thermics
* `ondu`: Energie ondulatoire
* `osci`: Energie oscillatoire

The available suffixes are:

* `Q`: Base quantity
* `E`: Effort
* `F`: Flow
* `I`: Impulse
* `C`: Capacitance
* `Cinv`: Elastance
* `L`: Inductance
* `Linv`: Reluctance
* `G` : Conductance
* `R`: Resistance

For example, the tension variable, which is the effort (`E`) of the electrodynamics variety (`elec`) will be written: `\elecE`.


### Example of code for a generic graph

    \begin{formalgraph}
     \base{$\geneQ$}    \baselabel{Quantite\\ de base}
     \effort{$\geneE$}  \effortlabel{Effort}
     \flow{$\geneF$}    \flowlabel{Flot}
     \impulse{$\geneI$} \impulselabel{Impulsion}

     \capac{Capacitance}
     \conduc{Conductance}
     \induc{Inductance}

     \evoL{Evolution\\ temporelle}
     \evoR{Evolution\\ temporelle}
    \end{formalgraph}




## Usage for 3D graphs (space distributed variables)
Put in the preamble of your document:

`\usepackage{formalgraphs3d}`

TODO
